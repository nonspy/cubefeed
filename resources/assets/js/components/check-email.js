$(function(window){
    "use strict";

    var validation  = {
        /**
         * Validate user provided Name. Accepts all lang diacritics,
         * no numbers and special chars
         *
         * @param $input
         */
        validateName: function($input) {
            $input.on("focusout", function() {
                var regex = /^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/,
                    value = $input.val();

                validation.setValidationStatus($input, regex.test(String(value)));
            });
        },


        /**
         * Validate user password. Minimum 8 chars including capitals and numbers.
         *
         * @param $input
         */
        validatePassword: function($input) {
            $input.on("focusout", function() {
                var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/,
                    value = $input.val();

                validation.setValidationStatus($input, regex.test(String(value)));
            });
        },


        /**
         * Validate password confirmation matches provided password
         *
         * @param $input
         * @param $password
         */
        validateConfirmPassword: function($input, $password) {
            $input.on("input propertychange", function() {
                var value = $input.val(),
                    passwordValue = $password.val();

                validation.setValidationStatus($input, value === passwordValue ? true : false);
            });
        },


        /**
         * Validate email and check if it already exists in DB
         *
         * @param $input
         */
        validateEmail: function($input) {
            $input.on("focusout", function() {
                var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    value = $input.val(),
                    validate = regex.test(String(value).toLowerCase());

                if (validate) {
                    validation.checkEmailExists(value, function(exists) {

                        validation.setValidationStatus($input, !exists);
                    });

                } else {
                    validation.setValidationStatus($input, false);
                }
            });
        },


        /**
         * Set field validated or not and if all is validated then show register button
         *
         * @param $input
         * @param isValidated
         */
        setValidationStatus: function($input, isValidated) {
            if (!isValidated) {
                $input
                    .parent()
                    .removeClass("validate-success")
                    .addClass("validate-fail");
            } else {
                $input
                    .parent()
                    .removeClass("validate-fail")
                    .addClass("validate-success");
            }

            validation.changeButtonStatus($(".registration-form button[type=submit]"));
        },


        /**
         * Check if provided email already exists in DB
         *
         * @param email
         * @param callback
         */
        checkEmailExists: function(email, callback) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.post( "/check-email", {
                "email" : email
            })
            .done(function(object) {
                callback(object.exists);
            })
            .fail(function() {
                alert( "Fail checking email." );
                callback(false);
            });
        },


        /**
         * Set register button disabled or enabled
         *
         * @param $button
         */
        changeButtonStatus: function($button) {
            var $validateElements = $(".validate"),
                validated = true;

            $validateElements.each(function() {
                if (!$(this).hasClass("validate-success")) {
                    validated = false;
                }
            });

            $button.attr("disabled", !validated);
        }
    };



    if ($(".registration-form .validate").length) {
        var $validation = $(".registration-form .validate");

        $validation.each(function(index, item) {
            var $input = $(item).find(".validate-input");

            switch ($input.attr("name")) {
                case "name":
                case "surname":
                    validation.validateName($input);
                    break;
                case "email":
                    validation.validateEmail($input);
                    break;
                case "password":
                    validation.validatePassword($input);
                    break;
                case "password_confirmation":
                    validation.validateConfirmPassword($input, $validation.find("input[name=password]"));
                    break;
            }
        });

    }
});
