<?php

return [
    'Login' => 'Ieiet',
    'Signup' => 'Reģistrēties',
    'Goto-feed' => 'Parādīt ziņas',
    'Description' => 'Elementārs RSS ziņu agregators ar autentifikāciju, e-pasta verifikāciju un valodas maiņas iespēju.',
    'Action' => 'Sāc ar to ka ieej sistēma vai arī piereģistrējies ja vēl nēesi paspējis to izdarīt.',
];
