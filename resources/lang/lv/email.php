<?php

return [
    'Hi' => 'Sveicināti,',
    'Step-to-verify' => 'Tikai viens maziņš solis līdz sāksiet izmantot labāko RSS lasītāju pasaulē. Nospiediet pogu zemāk lai apstiprināt savu e-pastu.',
    'Confirm-your-email' => 'Apstiprināt e-pastu',
];
