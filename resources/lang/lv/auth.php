<?php

return [
    'Login' => 'Ieiet',
    'Remember-me' => 'Atcerēties mani',
    'Signup' => 'Pieregistrēties',
    'Name' => 'Vārds',
    'Surname' => 'Uzvārds',
    'Email-address' => 'E-mail adrese',
    'Password' => 'Parole',
    'Confirm-password' => 'Atkārtojiet paroli',
    'failed' => 'Pierakstīšanās neveiksmīga.',
    'throttle' => 'Pārāk daudz pierakstīšanas mēģinājumi. Lūdzu pamēģiniet pēc :seconds sekundēm.',
    'Email-verified' => 'Tavs e-pasts ir verificēts. Tagad vari ieiet sistēmā.',
    'Email-verified-already' => 'Tavs e-pasts jau ir verificēts. Tagad vari ieiet sistēmā.',
    'Email-cannot-be-identified' => 'Atvaino, tavs e-pasts netiek atpazīts.',
    'Email-sent' => 'Mēs aizsūtījām tev aktivācijas saiti. Pārbaudi savu e-pastu un nospied verifikācijas pogu.',
    'Email-need-to-be-confirmed' => 'Tev ir nepieciešams apstiprināt savu kontu. Mēs aizsūtījām tev aktivācijas saiti. Lūdzu pārbaudi savu e-pastu.',
    'Password-requirements' => 'Minimums 8 simboli, iekļaujot vismaz: vienu lielo burtu, vienu mazo burtu, vienu ciparu.',
];
