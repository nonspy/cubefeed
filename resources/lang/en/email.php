<?php

return [
    'Hi' => 'Hi,',
    'Step-to-verify' => 'Just one small step to start using the best feed ever. Click button below to confirm Your e-mail.',
    'Confirm-your-email' => 'Confirm your email',
];
