<?php

return [
    'Login' => 'Log in',
    'Signup' => 'Sign up',
    'Logout' => 'Log out',
];
