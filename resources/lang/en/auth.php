<?php

return [
    'Login' => 'Log in',
    'Remember-me' => 'Remember me',
    'Signup' => 'Sign up',
    'Name' => 'Name',
    'Surname' => 'Surname',
    'Email-address' => 'E-mail address',
    'Password' => 'Password',
    'Confirm-password' => 'Confirm password',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Email-verified' => 'Your e-mail is verified. You can now login.',
    'Email-verified-already' => 'Your e-mail is already verified. You can now login.',
    'Email-cannot-be-identified' => 'Sorry your email cannot be identified.',
    'Email-sent' => 'We sent you an activation code. Check your email and click on the link to verify.',
    'Email-need-to-be-confirmed' => 'You need to confirm your account. We have sent you an activation code, please check your email.',
    'Password-requirements' => 'Minimum 8 chars, including at least: one capital letter, one usual letter and one number.',
];
