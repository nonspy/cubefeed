<?php

return [
    'Login' => 'Log in',
    'Signup' => 'Sign up',
    'Goto-feed' => 'Go to feed',
    'Description' => 'Simple RSS feed parser with authentication, e-mail verification and languaging.',
    'Action' => 'Lets start with logging in or registering if you have not performed it yet.',
];

