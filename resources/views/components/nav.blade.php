<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <span class="lnr lnr-layers"></span> {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto mr-2">
                @guest
                    @if (Route::current()->getName() !== 'login')
                        <li>
                            <a class="nav-link" href="{{ route('login') }}">@lang('nav.Login')</a>
                        </li>
                    @endif
                    @if (Route::current()->getName() !== 'register')
                        <li>
                            <a class="nav-link" href="{{ route('register') }}">@lang('nav.Signup')</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <button id="navbarDropdown" class="btn btn-sm btn-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="lnr lnr-user"></span> {{ Auth::user()->name . ' ' . Auth::user()->surname}}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                @lang('nav.Logout')
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>

            <div class="nav-lang nav-item">
                <div class="btn-group">
                    <button class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="lnr lnr-earth"></span> {{ LaravelLocalization::getCurrentLocale() }}
                    </button>
                    <div class="dropdown-menu">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <a class="dropdown-item" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">{{$localeCode}}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>