@extends('layouts.app')

@section('page-title', '- Best feed ever')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1 class="mb-4">
                    <a href="{{ $feedLink }}" target="_blank">{{ $feedTitle }}</a> : {{ $feedDescription }}
                </h1>
                <div class="card-columns">
                    @foreach($feedItems as $feedItem)
                        <div class="card">
                            <div class="card-body clearfix">
                                <h2 class="card-title">
                                    <img class="float-left pr-3 " src="{{ $feedItem->get_enclosure()->link }}" alt="{{ $feedItem->get_title() }}">
                                    <a href="{{ $feedItem->get_permalink() }}" target="_blank">{{ $feedItem->get_title() }}</a>
                                </h2>
                                <p class="card-text">
                                    {{ $feedItem->get_description() }}
                                </p>
                                <small class="float-left mt-2">
                                    <span class="lnr lnr-clock"></span> Posted on {{ $feedItem->get_date('j F Y | g:i a') }}
                                </small>
                                <a href="{{ $feedItem->get_permalink() }}" class="btn float-right btn-outline-primary pull-right" target="_blank">@lang('feed.read_more') <span class="lnr lnr-arrow-right"></span></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
