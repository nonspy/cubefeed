@extends('layouts.app')

@section('body-class', 'intro')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="jumbotron">
                    <h1 class="display-1">
                        <span class="lnr lnr-layers"></span> {{ config('app.name', 'Cubefeed') }}
                    </h1>
                    <h2>@lang('intro.Description')</h2>
                    <hr class="my-4">
                    @guest
                        <h4>@lang('intro.Action')</h4>

                        <p class="lead pt-3">
                            <a class="btn btn-primary btn-lg mb-3 mr-3" href="{{ url('login') }}" role="button">@lang('intro.Login')</a>
                            <a class="btn btn-outline-primary btn-lg mb-3" href="{{ url('register') }}" role="button">@lang('intro.Signup')</a>
                        </p>
                    @else
                        <p class="lead pt-3">
                            <a class="btn btn-primary btn-lg" href="{{ url('feed') }}" role="button">@lang('intro.Goto-feed')</a>
                        </p>
                    @endguest
                </div>
            </div>
        </div>
    </div>
@endsection