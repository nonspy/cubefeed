<?php

Route::post('/check-email', ['uses'=>'Auth\RegisterController@checkEmail']);
Route::get('/verify-email/{token}', 'Auth\RegisterController@verifyUser');

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ],
    function() {

        Route::get('/', function () {
            return view('index');
        });

        Auth::routes();

        Route::get('/feed', 'HomeController@index')->name('feed');

    }
);
