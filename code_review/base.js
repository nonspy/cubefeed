/* Mūsu funkcijas labāk ievrapot atsevišķajā scope lai nevar piekļūt no global un arī izčakarēt ar js ekstensijām
	vai citiem failiem. Ja mēs izmantojam jQ būtu labi liket iekš DOM ready uzreiz (jo skripti atrodas head-ā): $(function(){});
	Tad attiecīgi mūsu onclick funkcijas izsaukšana nestrādātu un būtu jākabina window globālajam mainīgajam
 */


function renderErrors(errors) {
	// Es izmantotu forEach for vietā, nav jārezolvo .length
	for (var i=0; i < errors.length; i++) { // Es izmantotu atstarpes arī starp i = 0;
		$("#errors").append("<div> &raquo; " + errors[i] + "</div>").show();
		/* Ja izmantojam loop $("#errors") jābūt deklarēts jau iepriekš un
		ierakstīts mainīgajā, lai katru reizi nav jāiet cauri DOM-am.
		Par &raquo; jau minīju html-ā. Es izmantotu div klasi ar before/after ikonām vai to pāsu pseido ar content: "\00BB";
		Katru iterāciju piemērot metodi show() arī nav korekti es darītu to pēc loop a pirms es ieliktu arī .empty()
		lai izdzēst jau esošus elementus. Nu un pieļaujot ka js būs minificēts/obfuscēts rakstītu katru metodi jaunajā rindiņā:
		$("#error")
		.empty()
		.append()
		.show()

		Labā prakse paredz arī pievienojama elementa deklarēšanu pirms append un tad tam atsevišķi pielikt classname
		un saturu, bet idejiski var atstāt arī esošu variantu jo jQ tas ir labi atstrādāts.
		 */
	};
	// Semikols beigās? Kkas jauns. Nav jābūt
}

function validateForm() {
	/* Kāpēc rezult nav deklarēts? Es izmantotu vienu var un deklarētu katru mainīgu jaunajā rindiņā */
	result = true; // Result nekur netiek izmantots
	var errors = [];

	/* Tagad nez kāpēc izmantojas pure JS nevis jQ. Ja jQuery ir pieslēgts un kautkur jau tiek izmantots,
	kāpēc lai te arī to neizmantot? Tiem if varētu izmantot atsevišķo funkciju vai arī vismaz piedeklarēt
	document kā mainīgu un izmantot to */
	if (document.getElementById("full-name").value == '') { // Izmantojam strict === lai nelikt js mašīnai pielīdzināt tipus kad nav nepieciešams
		// Es izmantotu pārbaudei .value.length
		errors.push("Lauks 'Vārs, Uzvārds' ir jānorāda obligāti"); // Typo: Vārs->Vārds :) Kā arī es neliktu quotes bez backslash
		result = false; // Lieks
	}
	if (document.getElementById("telefonanr").value == '') {
		errors.push("Lauks 'Telefona numurs' ir jānorāda obligāti");
		result = false; // Lieks
	}
	if (document.getElementById("zinojums").value == '') { // Par LV jau minēju HTML-ā
		errors.push("Lauks 'Ziņojums' ir jānorāda obligāti");
		result = false; // Lieks
	}

	if (errors[0] == undefined) { // Es pārbaudtu ar if(!errors.length), kas ir ātrāk un korektāk
		return true;
	} else {
		renderErrors(errors);
		return false;
	}
}

/* Absolūti nevajadzīga metode */
window.onload = function() {
	null;
}