<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Feeds;

class HomeController extends Controller
{

    /**
     * Set default RSS feed URL
     *
     * @var string
     */
    protected $feedUrl = 'http://www.delfi.lv/rss/?channel=delfi';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $feed = Feeds::make($this->feedUrl);

        // Check
        $feedDescription = $feed->get_description();
        $feedLink = $feed->get_permalink();
        $feedTitle = $feed->get_title();
        $feedItems = $feed->get_items();

        return view('feed', compact('feedDescription', 'feedLink', 'feedTitle', 'feedItems'));
    }


}
